\label{chapter:CP}
This chapter introduces the concept of constraint programming. It presents the two components of the paradigm then demonstrates how it can be extended to optimization problems.

\section{Principle}
Constraint programming (CP) is a programming paradigm that is composed of two main components, the modeling and the search. 

\subsection{Modeling}
Modeling aims at representing a mathematical problem, and is done using variables and constraints. Variables have an initial domain, and constraints state relations between these variables, or restrictions on values a variable can take. An example of variables is given in Equation~\ref{eq:variables}, and a constraint over these variables is given in Equation~\ref{eq:constraint}.

\begin{equation}\label{eq:variables}
    dom(i) = \{0,1,2,3\},\ dom(j) = \{2,3\}
\end{equation}
\begin{equation}\label{eq:constraint}
    i \neq j
\end{equation}

The goal of a \textbf{CP Solver} is to bind each variable to one value from their domain while respecting every constraint that is expressed over them.

\subsection{Search}
\label{subsection:cp_search}
Search is carried in two steps: propagation and branching.

The propagation consists of the removal of inconsistent values from the domain of each variable. Consistency can be defined in multiple ways (\textit{e.g.} bound consistency or domain consistency). The consistency level is a trade off between the propagation speed and the level of filtering that is made on the domains. For instance, bound consistency is reached faster than domain consistency but removes fewer values from the domains of the variables.

Branching consists of taking decisions on variables in order to explore the search space. When a decision is made, propagation takes place and reverberates that decision onto domains of the other variables. If this results in an inconsistency, \textit{e.g.} a domain being emptied, the branching algorithm backtracks and takes another decision.

Branching can be done multiple ways, some of them represented in Figure~\ref{fig:branching}. Decisions are generally guided by two heuristics. 
\begin{enumerate}
    \item Variable heuristic: which variable the search should select next~;
    \item Value heuristic: which value of the variable the branching should act upon (bind the variable to it or split the domain based on it, for instance).
\end{enumerate}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.85\textwidth]{images/branching.png}
    \caption{Examples of branching decisions}
    \label{fig:branching}
\end{figure}

 This branching can be automatic, with the practitioner only having to model its problem and the search being carried automatically, applying techniques that are selected using machine learning techniques, for instance. This is the case for CP Optimizer\cite{cp_optimizer}, where conditional time-interval variables, one of the main focus of this thesis, were first introduced by Laborie et al.\cite{laborie_reasoning_2008,laborie_reasoning_2009}. Obviously, CP Optimizer also lets the user use a customized search.
 
 In some other solvers, branching has to be explicitly described by the user. This is the case for OscaR\cite{oscar}. It is therefore possible to specialize the search for the problem at hand. The branching can be written from scratch, but some often-used techniques are directly available as well.

\section{Constraint optimization problem}

Constraint programming can also be used in optimization problems. A problem where the only goal is to find a feasible solution is called a constraint satisfaction problem (CSP), while a problem where the goal is to optimize some objective is called a constraint optimization problem (COP). For this, the algorithm is given some objective $o$. In the case of minimization, when the search finds a feasible solution where $o = o_1$, it is restarted with a new constraint $o < o_1$. Each time it finds a solution, it tightens the objective until the whole search space is explored or a stopping criterion is met. 