\label{chap:experiments}
This chapter evaluates the performances of the CP model presented in the last chapter through different experiments. Practically, the results given by the CP model are compared with the one given by classical scheduling strategies, then the CP model is tested on critical situations and its scalability is evaluated. 

All the tests are performed on the stations of Wavre and Ottignies (Appendix~\ref{appendix:ottignies}). This layout contains 304 track segments, 69 signals, 13 platforms, 478 itineraries and 595 possible routes. The layout was directly extracted from files provided by the Belgian railways.

The cost of rerouting a train to another platform is always equal to 0 on the studied stations as they are encoded in a way that does not provide any flexibility to do such things (the platforms are located either as first or last element of the itineraries, where no rerouting through other platforms are possible). Even though it was tested on toy examples on our part, further work on the extraction of stations needs to done in order to test this.\\


Three greedy approaches simulated via discrete event simulation are compared against CP:
\begin{itemize}
    \item \textbf{First Come First Served (FCFS)}: this strategy gives the priority to the trains according to their arrival time.
    \item \textbf{Highest Delay First Served (HDFS)}: this strategy gives priority to the trains according to their planned completion time.
    \item \textbf{Highest Priority First Served (HPFS)}: this strategy gives priority to the trains according to their category, as explained in~\ref{subsection:init_model_hetero}.
\end{itemize}

Three meta-parameters are considered during the experiments: 
\begin{enumerate}
    \item The \textit{time horizon} defines an upper bound on the estimated arrival time of the trains. Three time horizons are considered in the experiments: 30, 60 and 120 minutes.
    \item The \textit{number of trains} gives the number of trains which will be scheduled to arrive at the stations during the time horizon specified. We experimented our model with 5, 10, 15, 20, 25 and 30 trains.
    \item The \textit{decision time} is the number of seconds the railway operator have at disposal to take a decision. Although it is dependent on the situation, Rodriguez\cite{rodriguez_constraint_2007} suggests a time limit of 3 minutes to be used in reasonable conditions. 
\end{enumerate}

Experiments have been performed with the assumption that only feasible schedules are provided to the model. That is, when both the model and the discrete event simulator built to simulate the classical strategies finds a solution. Heterogeneous and homogeneous random schedules were generated.\\

The experiments have been realised on a Dell XPS 13 9360 with a 2.5GHz Intel Core i5 processor (4 CPUs) and 8Gb of RAM at 1866 MHz using a 64 Bits OpenJDK Server VM 1.8 (by JetBrains) on Windows 10 Family edition V1803. The model was implemented with the Java API of CP Optimizer V12.7.1 and the search was performed with four workers.


    
    
\section{Homogeneous traffic}
\label{section:results_homo}
This section assesses the performances of the model presented in Chapter~\ref{chap:new_model} when considering homogeneous traffic. The number of passengers and categories of the trains are not considered here. Hence, Equation~\ref{eq:obj_homo_platforms} is used as objective function. \\

Each configuration of meta-parameters is repeated 100 times with a random schedule and 3 minutes of decision time. The various values are generated with a uniform distribution. For example, for a schedule with 10 trains and an horizon of 120 minutes starting at 2pm, the program randomly chooses multiple consecutive itineraries \textit{i.e.} a random journey and if possible a platform to stop at per itinerary with a random stopping time. A random length of the train in the interval $[0, 120)$\footnote{The units of distance are relative to the layout of the station. The layout and data related to the train station used in the experiments can be found here: \url{https://github.com/ffelten/opendata}} and a random speed in $[35, 47)$ units of distance per minute are also chosen. The estimated arrival time and departure time are generated randomly in the interval $[2pm, 4pm]$. The planned completion time is set to the sum of the estimated arrival time and the number of itineraries times a constant factor $c$ \textit{i.e.} $pct_t = eat_t + N^j_t\times c$.
\\



% Please add the following required packages to your document preamble:
% \usepackage{multirow}
% \usepackage[table,xcdraw]{xcolor}
% If you use beamer only pass "xcolor=table" option, i.e. \documentclass[xcolor=table]{beamer}
\begin{table}[]
\centering
\caption{Results of CP and classical approaches for an homogeneous traffic and 3 minutes of decision time: delays, number of improvements and optimal solutions}
\label{tab:results_homo_delays}
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
                                   &                                      & \multicolumn{3}{c|}{\parbox{3.5cm}{\centering \textbf{Avg. delay (min.)}}}                  &                                        &                                        \\ \cline{3-5}
\multirow{-2}{*}{{\textbf{Horizon}}} & \multirow{-2}{*}{{\parbox{1cm}{\centering \textbf{\# trains}}}} & {\parbox{1.5cm}{\centering \textbf{FCFS}}} & {\parbox{1.5cm}{\centering \textbf{HDFS}}} &
{\parbox{1.5cm}{\centering \textbf{CP}}} &\multirow{-2}{*}{\parbox{1.5cm}{\centering \textbf{POS (x/100)}}} & \multirow{-2}{*}{\parbox{1.5cm}{\centering \textbf{OPT (x/100)}}} \\ \hline \hline

                                   & 30                                   & 1304.0        & 1303.0        & \cellcolor[HTML]{C0C0C0}863.12      & 100                                    & 0                                      \\
                                   & 25                                   & 900.0         & 906.0         & \cellcolor[HTML]{C0C0C0}562.59      & 100                                    & 0                                      \\
                                   & 20                                   & 660.0         & 665.0         & \cellcolor[HTML]{C0C0C0}411.81      & 100                                    & 0                                      \\
                                   & 15                                   & 348.0         & 347.0         & \cellcolor[HTML]{C0C0C0}215.72      & 100                                    & 20                                     \\
                                   & 10                                   & 143.0         & 146.0         & \cellcolor[HTML]{C0C0C0}108.59      & 100                                    & 62                                     \\
\multirow{-6}{*}{\begin{turn}{90}30 minutes\end{turn}}       & 5                                    & 67.86         & 66.72         & \cellcolor[HTML]{C0C0C0}38.32       & 100                                    & 99                                     \\ \hline \hline
                                   & 30                                   & 1271.2        & 1277.35       & \cellcolor[HTML]{C0C0C0}735.87      & 100                                    & 0                                      \\
                                   & 25                                   & 871.57        & 879.69        & \cellcolor[HTML]{C0C0C0}474.31      & 100                                    & 0                                      \\
                                   & 20                                   & 616.71        & 623.65        & \cellcolor[HTML]{C0C0C0}340.69      & 100                                    & 2                                      \\
                                   & 15                                   & 355.35        & 357.14        & \cellcolor[HTML]{C0C0C0}199.21      & 100                                    & 18                                     \\
                                   & 10                                   & 181.93        & 182.76        & \cellcolor[HTML]{C0C0C0}99.15       & 100                                    & 70                                     \\
\multirow{-6}{*}{\begin{turn}{90}1 hour\end{turn}}           & 5                                    & 65.47         & 65.88         & \cellcolor[HTML]{C0C0C0}40.31       & 100                                    & 99                                     \\ \hline \hline
                                   & 30                                   & 1021.94       & 1027.49       & \cellcolor[HTML]{C0C0C0}527.19      & 99                                     & 0                                      \\
                                   & 25                                   & 809.17        & 803.81        & \cellcolor[HTML]{C0C0C0}424.48      & 100                                    & 1                                      \\
                                   & 20                                   & 485.09        & 484.22        & \cellcolor[HTML]{C0C0C0}246.67      & 100                                    & 12                                     \\
                                   & 15                                   & 312.7         & 314.82        & \cellcolor[HTML]{C0C0C0}158.97      & 100                                    & 43                                     \\
                                   & 10                                   & 161.35        & 163.21        & \cellcolor[HTML]{C0C0C0}87.5        & 100                                    & 81                                     \\
\multirow{-6}{*}{\begin{turn}{90}2 hours\end{turn}}          & 5                                    & 61.52         & 61.73         & \cellcolor[HTML]{C0C0C0}38.65       & 100                                    & 100                                    \\ \hline
\end{tabular}
\end{table}

Table~\ref{tab:results_homo_delays} shows a part of the results of these experiments. The delays indicated in the first table are the arithmetic means over all the measures per configuration: each line contains the average delay of each configuration. POS indicates the number of times CP provides an equal or better solution than both other strategies while OPT indicates the number of times CP finds an optimal result.

As in~\cite{cappart_rescheduling_2017}, it appears that CP gives better solutions almost all the time (more than 99\% of the cases). The model also gives optimum solution oftenly (more than 75\% of the cases) for instances with 10 or less trains. Naturally, the average delay grows with the number of trains and decreases when enlarging the horizon.\\

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table}[]
\centering
\caption{Results of CP and classical approaches for an homogeneous traffic and 3 minutes of decision time: improvement ratios}
\label{tab:results_homo_improvement_ratios}
\begin{tabularx}{\textwidth}{|c|c|c|c|c|c|c|c|}
\hline
\multirow{2}{*}{\parbox{1cm}{\centering \begin{turn}{90}\textbf{Horizon}\end{turn}}} & \multirow{2}{*}{\textbf{\parbox{1.3cm}{\centering \# trains}}} & \multicolumn{2}{c|}{\parbox{3.5cm}{\centering \textbf{Avg. Improvement Ratio (\%)}}} & \multicolumn{2}{c|}{\parbox{3.5cm}{\centering \textbf{Best Improvement Ratio (\%)}}} & \multicolumn{2}{c|}{\parbox{3.5cm}{\centering \textbf{Worst Improvement Ratio (\%)}}} \\ \cline{3-8} 
                                  &                                     & \parbox{1.7cm}{\vspace{2mm}\centering \textbf{CP vs FCFS}\vspace{2mm}}         & \parbox{1.7cm}{\vspace{2mm}\centering \textbf{CP vs HDFS}\vspace{2mm}}         & \parbox{1.7cm}{\vspace{2mm}\centering \textbf{CP  vs FCFS}\vspace{2mm}}         & \parbox{1.7cm}{\vspace{2mm}\centering \textbf{CP vs HDFS}\vspace{2mm}}        & \parbox{1.7cm}{\vspace{2mm}\centering \textbf{CP vs FCFS}\vspace{2mm}}         & \parbox{1.7cm}{\vspace{2mm}\centering \textbf{CP  vs HDFS}\vspace{2mm}}         \\ \hline \hline
\multirow{6}{*}{\begin{turn}{90}30 minutes\end{turn}}       & 30                                  & 38.39                       & 37.88                       & 62.83                        & 57.92                      & 1.16                        & 2.27                         \\
                                  & 25                                  & 41.17                       & 42.43                       & 67.43                        & 70.68                      & 11.68                       & 11.41                        \\
                                  & 20                                  & 43.01                       & 43.68                       & 70.02                        & 67.37                      & 10.8                        & 19.29                        \\
                                  & 15                                  & 48.08                       & 48.44                       & 76.38                        & 74.25                      & 13.18                       & 14.92                        \\
                                  & 10                                  & 45.46                       & 45.81                       & 80.52                        & 80.52                      & 9.77                        & 9.77                         \\
                                  & 5                                   & 47.96                       & 46.82                       & 100.0                        & 100.0                      & 0.0                         & 0.0                          \\ \hline \hline
\multirow{6}{*}{\begin{turn}{90}1 hour\end{turn}}           & 30                                  & 43.41                       & 43.73                       & 69.35                        & 67.94                      & 11.36                       & 11.36                        \\
                                  & 25                                  & 46.77                       & 47.21                       & 71.12                        & 71.12                      & 16.37                       & 16.37                        \\
                                  & 20                                  & 46.26                       & 46.9                        & 69.35                        & 71.54                      & 16.43                       & 15.71                        \\
                                  & 15                                  & 45.77                       & 45.87                       & 74.66                        & 74.66                      & 15.44                       & 15.44                        \\
                                  & 10                                  & 46.54                       & 46.76                       & 100.0                        & 100.0                      & 16.18                       & 16.18                        \\
                                  & 5                                   & 41.35                       & 41.5                        & 100.0                        & 100.0                      & 0.0                         & 0.0                          \\ \hline \hline
\multirow{6}{*}{\begin{turn}{90}2 hours\end{turn}}          & 30                                  & 49.56                       & 49.73                       & 77.5                         & 76.89                      & -2.17                       & -2.17                        \\
                                  & 25                                  & 49.48                       & 49.08                       & 82.25                        & 82.75                      & 14.56                       & 14.56                        \\
                                  & 20                                  & 49.76                       & 49.73                       & 70.0                         & 70.0                       & 11.31                       & 11.75                        \\
                                  & 15                                  & 50.36                       & 50.47                       & 76.72                        & 76.72                      & 17.87                       & 17.87                        \\
                                  & 10                                  & 45.12                       & 45.41                       & 90.91                        & 90.91                      & 14.29                       & 14.29                        \\
                                  & 5                                   & 41.79                       & 41.95                       & 100.0                        & 100.0                      & 0.0                         & 0.0                          \\ \hline
\end{tabularx}
\end{table}

Table~\ref{tab:results_homo_improvement_ratios} concerns the other part of these experiments. It gives the improvement ratio when comparing our CP model with the classical approaches: the average, best and worst improvement of CP against FCFS and of CP against HDFS are reported in each line. 

In this table, the average improvement ratio is above 40\% in all the scenarios except one. The best improvement ratio reaches sometimes 100\%, that is when CP gives a solution with no delay while the other strategy gives a solution with delay. On the other hand, the worst improvement ratio has a value of 0 in every scenario implying 5 trains, that is when CP gives the same solution as the greedy approach.




\section{Heterogeneous traffic}
This section studies the performances of the model on an heterogeneous traffic. Equation~\ref{eq:obj_hetero_platforms} is used as objective function. The number of passengers and categories of trains are then considered in these experiments.\\

The scenarios are generated randomly as in the previous section for all the common parts. The category of the train is chosen in a random fashion according to a uniform distribution among the four possibilities. The number of passenger is chosen the same way in the interval $[0, 200)$, for passenger and correspondence trains. \\

As explained in~\cite{cappart_rescheduling_2017}, only HPFS deals with an heterogeneous traffic. The optimisations are performed one after the other for each part of the objective function as advised in~\cite{cp_optimizer_user_manual_1987}. The search time is allocated according to the priority of the categories with an upper bound of 60\% of the remaining time at each step (except the last one which uses all the remaining time).\\

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table}[]
\centering
\caption{Results of CP and classical approach (HPFS) for an heterogeneous traffic and 3 minutes of decision time}
\label{tab:results_hetero}
\begin{tabular}{|c|c|c|c|}
\hline
\textbf{Horizon}            & \textbf{Trains} & \textbf{POS} & \textbf{OPT} \\ \hline \hline
\multirow{6}{*}{\begin{turn}{90}30 minutes\end{turn}} & 30              & 99           & 0            \\
                            & 25              & 100          & 0            \\
                            & 20              & 100          & 3            \\
                            & 15              & 99           & 29           \\
                            & 10              & 100          & 66           \\
                            & 5               & 98           & 96           \\ \hline \hline
\multirow{6}{*}{\begin{turn}{90}1 hour\end{turn}}     & 30              & 98           & 0            \\
                            & 25              & 98           & 1            \\
                            & 20              & 100          & 7            \\
                            & 15              & 99           & 36           \\
                            & 10              & 98           & 74           \\
                            & 5               & 99           & 99           \\ \hline \hline
\multirow{6}{*}{\begin{turn}{90}2 hours\end{turn}}    & 30              & 100          & 1            \\
                            & 25              & 99           & 7            \\
                            & 20              & 100          & 28           \\
                            & 15              & 100          & 56           \\
                            & 10              & 99           & 73           \\
                            & 5               & 100          & 99           \\ \hline 
\end{tabular}
\end{table}

Table~\ref{tab:results_hetero} reports the results of the experiments. Unlike in the previous section, the delays and improvement ratios are not reported in this table. This is motivated by the fact that it is subjective to define an improvement ratio in such traffic. Indeed, it is up to the railway operator to decide whether the reduction of the delay of one train of category C1 against 5 trains of a lower priority is worth it. As in~\cite{cappart_rescheduling_2017}, we consider that CP gives a better solution than HPFS if its delay per category is lexicographically lower than the solution provided by the greedy approach.

In this kind of traffic, the CP model gives a better or equal solution in almost all the cases as well (around 99\% of the cases). It also gives optimum solution in more than 75\% of the cases when considering 10 trains or less.


\section{Critical situations}

This section studies the variation of performances of the model when facing critical situations \textit{i.e.} with a small decision time. Concretely, the scenarios are generated the same way as with the homogeneous traffic. Two configurations of the model are launched on every scenario: one with 10 seconds of decision time (the critical CP model) and the other with 3 minutes (the uncritical model). Only scenarios where the critical CP gives a solution are considered.\\

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table}[]
\centering
\caption{Comparison between a critical CP model (10 seconds of decision time) and a normal CP model (3 minutes of decision time) on an homogeneous traffic}
\label{tab:critCP}
\begin{tabular}{|c|c|c|c|c|c|c|c|}
\hline
\parbox{1cm} {\centering \begin{turn}{90}\textbf{Horizon}\end{turn}}&
\textbf{\begin{tabular}[c]{@{}c@{}} \#\\ Trains \end{tabular}} & \textbf{\begin{tabular}[c]{@{}c@{}}Avg.\\ Delay\\ crit. CP\end{tabular}} & \textbf{\begin{tabular}[c]{@{}c@{}}Avg.\\ Delay\\ CP\end{tabular}} & \textbf{\begin{tabular}[c]{@{}c@{}}Avg.\\ Improvement\\ Ratio (\%)\end{tabular}} & \textbf{\begin{tabular}[c]{@{}c@{}}Best\\ Improvement\\ Ratio (\%)\end{tabular}} & \textbf{OPT$_{crit}$} & \textbf{OPT} \\ \hline\hline
\multirow{6}{*}{\begin{turn}{90}30 minutes\end{turn}}                       & 5                  & 51.6                                                                 & 51.01                                                            & 0.92                                                                & 42.5                                                                & 83                 & 99           \\
                                             & 10                 & 122.36                                                               & 116.0                                                            & 4.23                                                                & 50.0                                                                & 15                 & 62           \\
                                             & 15                 & 280.07                                                               & 247.82                                                           & 8.92                                                                & 52.93                                                               & 5                  & 12           \\
                                             & 20                 & 601.39                                                               & 445.92                                                           & 23.0                                                                & 54.37                                                               & 0                  & 1            \\
                                             & 25                 & 933.9                                                                & 669.29                                                           & 26.18                                                               & 57.58                                                               & 0                  & 0            \\
                                             & 30                 & 1398.9                                                               & 987.1                                                            & 27.34                                                               & 68.41                                                               & 0                  & 0            \\ \hline\hline
\multirow{6}{*}{\begin{turn}{90}1 hour \end{turn}}                       & 5                  & 36.67                                                                & 36.59                                                            & 0.07                                                                & 5.49                                                                & 89                 & 99           \\
                                             & 10                 & 99.53                                                                & 97.35                                                            & 1.77                                                                & 27.03                                                               & 41                 & 69           \\
                                             & 15                 & 233.96                                                               & 203.31                                                           & 8.56                                                                & 54.72                                                               & 4                  & 23           \\
                                             & 20                 & 462.07                                                               & 364.15                                                           & 17.78                                                               & 64.71                                                               & 1                  & 2            \\
                                             & 25                 & 732.74                                                               & 539.92                                                           & 22.73                                                               & 54.77                                                               & 0                  & 0            \\
                                             & 30                 & 1128.28                                                              & 831.08                                                           & 25.36                                                               & 56.78                                                               & 0                  & 0            \\ \hline \hline
\multirow{6}{*}{\begin{turn}{90}2 hours \end{turn}}  & 5                  & 41.41                                                                & 41.29                                                            & 0.17                                                                & 17.39                                                               & 93                 & 100          \\
\multicolumn{1}{|l|}{}                       & 10                 & 90.07                                                                & 88.56                                                            & 1.39                                                                & 36.84                                                               & 56                 & 81           \\
\multicolumn{1}{|l|}{}                       & 15                 & 177.35                                                               & 163.23                                                           & 4.87                                                                & 36.91                                                               & 13                 & 38           \\
\multicolumn{1}{|l|}{}                       & 20                 & 311.7                                                                & 260.33                                                           & 12.33                                                               & 55.94                                                               & 2                  & 9            \\
\multicolumn{1}{|l|}{}                       & 25                 & 535.43                                                               & 401.76                                                           & 20.68                                                               & 58.96                                                               & 0                  & 0            \\
\multicolumn{1}{|l|}{}                       & 30                 & 703.73                                                               & 546.64                                                           & 19.69                                                               & 48.71                                                               & 0                  & 0            \\ \hline
\end{tabular}
\end{table}


Table~\ref{tab:critCP} recaps the results of the experiments where the critical model found a solution. As in Section~\ref{section:results_homo}, the average delay is the arithmetic mean of the delays over the 100 runs per configuration of meta-parameters. The improvement ratios quantifies by how much the uncritical CP model gives better solutions than the critical one. OPT indicates the number of times the non critical model provides an optimal solution whereas OPT$_{crit}$ gives the same figure for the critical one. The worst improvement ratio does not appear as it is always equal to zero.

It appears that the model overall behaves well with little decision time. On small instances (15 or less trains), the average improvement ratio is always less than 10\%. On bigger instances, it may be worthy to give more decision time if available as the average improvement is always over 10\% in those cases. Obviously, the number of optimal solutions increases when the model benefits of more decision time.

By comparing the average improvement ratios in Tables~\ref{tab:results_homo_improvement_ratios} and \ref{tab:critCP}, it appears that in general the critical CP model gives better results than classical heuristics. 

\section{Scability}

\begin{figure}
    \centering
    \includegraphics[scale=0.40]{images/scalability.png}
    \caption{Evolution of the computing time needed to obtain a first solution over 100 random homogeneous instances with an horizon of 30 minutes}
    \label{tab:scalability}
\end{figure}

It has been showed in~\cite{cappart_rescheduling_2017} that there is no real gain in increasing the decision time to more than 3 minutes for the initial model. The previous sections shown the model to be efficient even on multiple stations. In this section, we are interested in the minimum amount of time the model needs to find a solution.\\



Figure~\ref{tab:scalability} gives the evolution of the computing time needed to have a solution for an instance over 100 random homogeneous scenarios as a function of the size of the trains. The average, maximum and minimum time over all the tests are reported. The horizon is set to 30 minutes as it generates the most constrained scenarios (because of the density of trains within a short time window). Only the cases where a solution is found within 3 minutes are considered. 

The model performs well in average in terms of scalability: the average solving time needed to obtain a first solution is less than 20 seconds for up to 30 trains. However, some cases occur where the model needs up to 3 minutes time to find a solution. This leads us to think that some instances may lead to timeouts. Some further experiments with real data would be interesting to consider to assess this.



\section{Reproducibility}
Files and information to reproduce our results are available on \url{https://github.com/ffelten/opendata}. Using this document and the files provided on GitHub, it is possible to build a model and perform the same experiments as we did. 