\label{chap:init_model}
This chapter presents the model proposed by Cappart and Schaus\cite{cappart_rescheduling_2017} to solve timetable rescheduling problems. It is a basis to the final model presented in the next chapter.

\begin{figure}
    \centering
    \includegraphics[scale=0.19]{images/toy_station_itineraries.png}
    \caption{Layout of a fictive train station}
    \label{fig:toy_station_itineraries}
\end{figure}

% TMS gives info in case of unfeasibility of schedule to TC's operator which takes the decision and forwards to local signalling and switching tower. The latter adapts signals and switches and is responsible for the security.

    \section{Signaling principles}
    The Belgian railways relies on the Traffic Control (TC) to schedule all of its trains. Ordinarily, the traffic is automatically regulated by a software system called Traffic Management System (TMS) based on the predefined timetables and rules. 
    
    However, in case of disturbances or disruptions, the TMS detects future conflicts and notifies the TC. The decisions related to rescheduling and rerouting of trains are then done manually by an operator. The actions to be made on the field are forwarded to the local signalling and switching towers which are responsible for the security.
    
    The purpose of the model presented in this chapter is to help the operator to take a decision in such situation.\\
    %The National Railway Company of Belgium is at the forefront of the use of the European Train Control System (ETCS). Thanks to this system, it is easy to collect precious information about the trains such as their speed or position on railway tracks in real time and thus ensure that the system is safe with the Traffic Management System (TMS). 
    
    A layout of a fictive train station as presented in~\cite{cappart_rescheduling_2017} can be found on Figure~\ref{fig:toy_station_itineraries}. A train station is composed of multiple elements:
    \begin{itemize}
        \item The \textbf{track segments} (\textit{e.g.} \textit{T1}) are portions of railway tracks. They are delimited by joints ($\dashv\vdash$) on the figure. Thanks to the TMS, it is possible detect on which track segments the trains are located at any moment.
        
        \item The \textbf{signals} (\textit{e.g.} \textit{S1}) are used to control the traffic. They can be either set to red if the train is not allowed to go further at the moment or to green if it is safe to pass through.
        
    \end{itemize}
    
Because the layouts also have movable switches, there are logical structures that also need to be considered:
\begin{itemize}
    \item The \textbf{routes} consist of paths that trains can follow in order to reach their destination. They are defined as a sequence of track segments and signals. For instance, \textit{R1} is a route having \textit{T4} as origin and \textit{T7} as destination. The following elements compose the path to follow to reach the destination from the origin: \textit{[T4,S2,T5,T2,T6]}. Note that the destination is not part of the path itself. Additionally, a route always contains a signal as second element. This signal is always red until the TMS decides to allow the train to continue its journey, this action is called a \textit{route activation}.

    After the activation of a route, all the track segments following the signal are reserved for the train. They are released sequentially after the train passed them. For instance, once the train has reached \textit{T2} and is not occupying \textit{T5} anymore, then the latter can be released.
    
    \item The \textbf{itineraries} correspond to the non physical paths between two locations within the station. It can be constituted by one or more routes. For example, in the Figure~\ref{fig:toy_station_itineraries}, we can see that \textit{R1} and \textit{R2} both lead to the accomplishment of \textit{IT1} (from \textit{T4} to \textit{T7}). Usually, a route is always preferred. However, in case of conflicts, the route causing the least amount of conflicts is  to be chosen.
\end{itemize}

    \section{Elements}
    This section explains how the problem was modelled by Cappart and Schaus~\cite{cappart_rescheduling_2017}. Remember that the goal here is to schedule the trains adequately to bring them to their destination. Practically, it consists of choosing for each train which route to use and when to activate it. For security purposes, the model should ensure that the elements of the train station are always used by at most one train.
    
        \subsection{Activities}
        \label{subsection:activities}
        The main components of the scheduling problems are activities. Basically, an activity \textit{A} is modelled with three variables, a start date \textit{s(A)}, a duration \textit{d(A)} and an end \textit{e(A)}. The activity is considered to be fixed when the domains of its variables are reduced to singletons. There are three kinds of activities that are considered in the model:
        
        \begin{itemize}
            \item The \textbf{itinerary activities} define a time interval during which the train is following an itinerary. Each train has one and only one itinerary activity to accomplish. $A^{t,it}$ corresponds to the activity for itinerary \textit{it} of train \textit{t}.
            \item The \textbf{route activities} define a time interval during which a particular route of an itinerary is followed by a train. $A^{t, it, r}$ stands for the route activity for route \textit{r} of itinerary \textit{it} related to train \textit{t}.
            \item The \textbf{element activities} correspond to the movements of the train along the route. Such activities are associated to track segments and signals. We write $A^{t,it,r}_{i}$ as the $i^{th}$ element activity for route \textit{r} of itinerary \textit{it} related to train \textit{t}. There are as many element activities as the number of components of the route. For instance, using the example of Figure~\ref{fig:toy_station_itineraries}, $A^{t, IT1, R1}_{1}$ is an element activity corresponding to the track segment \textit{T4} while $A^{t, IT1, R1}_{2}$ applies to \textit{S2}, etc.
        \end{itemize}

        A particularity of this model is that some activities are optional – they may or may not be executed. For example, if we want to accomplish \textit{IT1} in Figure~\ref{fig:toy_station_itineraries}, we could either use \textit{R1} or \textit{R2}. If one of them is chosen, then the activity corresponding to the other route will not be executed.
        Such activities can be easily modelled using the conditional time-interval variables presented in Chapter~\ref{chapter:CTI}. \\
        
        % These variables encapsulate the notion of optionality. Making it easier to model such type of scheduling problems. Moreover, it allows an efficient propagation through global constraints on  CTIs as well as an efficient search. \\
        
        
        % When an activity is fixed, it is either executed or not executed. If the activity is executed, it is considered as a classical activity, otherwise it is not considered by any constraint in the model and its variables' domains are reduced to bottom ($\bot$). This is modelled by the addition of a new boolean variable $x(A) \in {0,1}$ for each activity \textit{A}. Typically, $x(A) = 1$ if the activity is executed, 0 otherwise.\\
        
        %\begin{empheq}[box=\mymath]{equation*}
        %dom(var) =
        %\begin{cases}
        %    \{\bot\} \text{ if the event is not executed}\\[3mm]
        %   \{ [s, e) | s, e \in \mathbb{Z}, s \leq e\} \text{ otherwise}
        %\end{cases}
        %end{empheq}
        
        \begin{figure}
            \centering
            \includegraphics[scale=0.7]{images/breakdown_span_alternative.PNG}
            \caption{Breakdown structure of the alternative and span constraints on activities~\cite{cappart_rescheduling_2017}}
            \label{fig:breakdown_span_alternative}
        \end{figure}
        
        Figure~\ref{fig:breakdown_span_alternative} shows how the different activities are organised in the model. $A^{t, IT1}$ is mandatory as we need to ensure the train reaches its destination, the other activities are optional. The \texttt{alternative} constraint ensures that only one route will be chosen for itinerary \textit{IT1}. In other words, either $A^{t, IT1, R1}$ or $A^{t, IT1, R2}$ will be executed but not both. Furthermore, the start and end date of the chosen route is synchronised with the start and end dates of the itinerary activity. 
        
        The \texttt{span} constraint is used to synchronise the start of the route activity with the start of the first element activity of this route and its end with the last element activity of this route. For instance, $A^{t, IT1, R2}$ is synchronised with the start date of $A^{t, IT1, R2}_1$ and the end date of $A^{t, IT1, R2}_6$.  

        
        \subsection{Parameters} %(speed, number of passengers,…)

        \begin{table}
        \centering
        \caption{Parameters of trains and track segments in the model}
        \label{tab:params}
        \begin{tabularx}{\textwidth}{|c|c|c|X|}
        \hline
        \textbf{Entity}        & \textbf{Parameter}      & \textbf{Name} & \textbf{Meaning}                             \\ \hline
        \multirow{6}{*}{Train} & Speed                   & $spd_t$       & Speed of $t$                                 \\
                               & Passengers              & $p_t$         & Number of passengers of $t$                  \\
                               & Estimated Arrival Time  & $eat_t$       & When $t$ will arrive at the station          \\
                               & Earliest Start Time     & $est_t$       & Lower bound on the start time of $t$         \\
                               & Planned Completion Time & $pct_t$       & When $t$ is supposed to complete its journey \\
                               & Category                & $c_t$         & Category of $t$                              \\ \hline
        Track segment          & Length                  & $lgt_{ts}$    & Length of $ts$                               \\ \hline
        \end{tabularx}
        \end{table}
        A few parameters which characterise the trains and the track segments are considered in the model. Table~\ref{tab:params} recaps the meaning of each parameter.
        
        \subsection{Decision variables} %– how we use the activities and the parameters to make a model
        \label{subsection:decision_varaibles}
        As explained above, the goal of our model is to decide which route to execute and at what time for each train. There are a few more points to be considered in order to obtain a safe model:
        
        \begin{itemize}
            \item The end of an element activity must be synchronised with the start of the subsequent element activity along a route.
            \item An element activity can retain more than one resource. Indeed, when a train is on a track segment, it reserves the track segment as well as the next track segments on the route. For example, let us consider once again the route activity $A^{t, IT1, R1}$ depicted on Figure~\ref{fig:toy_station_itineraries}. The corresponding element activities are $A^{t, IT1, R1}_1$, $A^{t, IT1, R1}_2$, $A^{t, IT1, R1}_3$, $A^{t, IT1, R1}_4$ and $A^{t, IT1, R1}_5$. The resources used by these activities are $(T4, S2)$, $(S2)$, $(T5,T2,T6)$, $(T2,T6)$, $(T6)$, respectively. Note that the reservation goes no further than $S2$ for the two first activities: this represents the fact that the route activation has not occurred during these activities.
            
            \begin{figure}
                \centering
                \includegraphics[scale=0.2]{images/train_position.png}
                \caption{Position of the train on a station}
                \label{fig:train_position}
            \end{figure}
            
            The position of the train is given by the activity it is executing at the moment and its associated element, which corresponds to the first element of its resources. For instance, remember that during $A^{t, IT1, R1}_1$, $t$ is considered as on $T4$ while during $A^{t, IT1, R1}_2$, it is considered as in front of $S2$. This is shown on Figure~\ref{fig:train_position}. The first train shows the position of a train at the beginning of activity $A^{t,IT1,R1}_1$ whereas the second one shows its position at the end of $A^{t,IT1,R1}_1$ and the start of $A^{t,IT1,R1}_2$. The third train shows the end of the latter and the start of $A^{t,IT1,R1}_3$. 
    
        \end{itemize}
        
        From a scheduling point of view, the problem is to assign a unique value to each element activity. The decision variables and their domains for each train \textit{t}, itinerary \textit{it}, route \textit{r} and index \textit{i} are listed below:
        
        \begin{align}
        \label{eq:start}
            s(A^{t, it, r}_{i})
            \begin{cases}
            \in [eat_t, horizon] & \text{if $t$ is on track segment $ts$}\\[3mm]
            \in [est_t, horizon] & \text{if $t$ is in front of a signal}
            \end{cases}
        \end{align}
        
        Equation~\ref{eq:start} states that an activity cannot begin before the estimated arrival time of $t$. The horizon is an upper bound of the starting date of the activity which is fixed by the user.
        
        \begin{align}
        \label{eq:duration}
            d(A^{t, it, r}_{i})
            \begin{cases}
            = lgt_{ts} / spd_t & \text{if $t$ is on track segment $ts$}\\[3mm]
            \in [0, horizon] & \text{if $t$ is in front of a signal}
            \end{cases}
        \end{align}
        
        Equation~\ref{eq:duration} gives us the time needed to perform an element activity. Naturally, if $t$ is on a track segment, the duration of the element activity will be the time needed to pass through it. Otherwise, if $t$ is located in front of a signal, then the time it will have to wait is unknown.
        
        \begin{align}
        \label{eq:end}
            e(A^{t, it, r}_{i}) = s(A^{t, it, r}_{i}) + d(A^{t, it, r}_{i})
        \end{align}
        
        Equation~\ref{eq:end} is an implicit consistency constraint. It expresses the fact that the end date of an activity must be equal to the sum of its start date and its duration.
        
        \begin{align}
        \label{eq:presence}
            x(A^{t, it, r}_{i}) \in \{0, 1\}
        \end{align}
        
        Lastly, Equation~\ref{eq:presence} indicates that the activity is optional. 
    \section{Constraints}
    \label{section:init_model_constraints}
    This section describes the constraints that are used in the model. Let us first define $T$ as the set of trains, $IT_t$ as the set of possible itineraries for $t \in T$, $R_{it}$ the set of possible routes for $it \in IT_t$, $N_r$ as the number of element activities for a route $r \in R_{it}$ and $TS$ as the set of track segments within the station.
        \subsection{Precedence} % – element’s activities execute in order
        \label{subsection:precedence}
        \begin{align}
            \label{eq:precedence}
            e(A^{t, it, r}_{i}) = s(A^{t, it, r}_{i+1}) \hspace{5mm} \forall t \in T, \forall it \in IT_t, \forall r \in R_{it}, \forall i \in [1, N_r)
        \end{align}
        This constraint (Equation~\ref{eq:precedence}) enforces the element activities to be executed in order.
        
        \subsection{Execution consistency}% – if one element activity of a route is executed, then all are executed
        \begin{align}
            \label{eq:exec_consistency}
            x(A^{t, it, r}_{1}) \equiv x(A^{t, it, r}_{i}) \hspace{5mm} \forall t \in T, \forall it \in IT_t, \forall r \in R_{it}, \forall i \in ]1, N_r]
        \end{align}
        Because some routes lead to the achievement of the same itinerary, some of them are not chosen for a train. In this case none of the activities $A^{t,it,r}_i$ will be executed. Otherwise, all of them must be executed. It is expressed in Equation~\ref{eq:exec_consistency} - all or none of the element activities that compose a route activity are to be executed.
        \subsection{Alternative}
        \begin{align}
            \label{eq:alternative}
            \texttt{alternative}(A^{t, it}, \{ A^{t, it, r} | r \in R_{it} \}) \hspace{5mm} \forall t \in T, \forall it \in IT_t
        \end{align}
        
        This constraint was introduced by Laborie and Rogerie~\cite{laborie_reasoning_2008} and presented in Subsection~\ref{subsub:alternative}. It means that when the itinerary activity $A^{t,it}$ is executed, then exactly one of the route activities $A^{t, it, r}$ must be executed. Moreover, the start and end date of the itinerary and route activities must be synchronised.
        
        Let us notice that when $A^{t, it}$ is not executed, then none of the route activities can be executed. This is why all $A^{t, it}$ are specified as mandatory in the model in order to ensure that every train reaches its destination.
        
        \subsection{Span}
        
        \begin{align}
            \label{eq:span}
            \texttt{span}(A^{t, it, r}, \{ A^{t, it, r}_i | i \in [1, N_r] \}) \hspace{5mm} \forall t \in T, \forall it \in IT_t, \forall r \in R_{it}
        \end{align}
        
        Like \texttt{alternative}, this constraint was introduced in~\cite{laborie_reasoning_2008} but also presented in Subsection~\ref{subsub:span}. It states that an executed route activity $A^{t, it , r}$ must span over some of the executed element activities that compose the route. It also ensures that the start of the route activity is synchronised with the start of the first underlying element activity as well as the end of the route activity is synchronised with the end of the last element activity.
        
        Using this constraint, it is easy to model the fact that the duration of a route activity is equal to the time taken to cross all of its components.
        \subsection{noOverlap - unary resource}
        \label{subsection:noOverlap}
        
        An important part of the model is the security aspect. Indeed, it must be impossible for a train to move onto or reserve a track segment that is currently used by another one.
        
        Let us state that $ACT_{ts}$ is the set of all activities using the track segment $ts$:
        
        \begin{align}
            \label{eq:noOverlap}
            \texttt{noOverlap}(A |  A \in ACT_{ts} \}) \hspace{5mm} \forall ts \in TS
        \end{align}
        
        This constraint was introduced in~\cite{laborie_reasoning_2009}. Its semantic is presented in Subsection~\ref{subsub:nooverlap}. It states that the set of all executed activities in $ACT_{ts}$ must form a chain of non-overlapping intervals \textit{i.e.} any executed activity in the chain must finish before the start of the next executed activity.
    
        
        \subsection{Train Ordering consistency}
        \begin{figure}
            \centering
            \includegraphics[scale=0.7]{images/ordering_consistency.PNG}
            \caption{Two trains waiting on the same track segment~\cite{cappart_rescheduling_2017}}
            \label{fig:ordering_consistency}
        \end{figure}
        
        This constraint states that trains cannot overtake other trains if they are located on the same track segment. Such a situation is illustrated on Figure~\ref{fig:ordering_consistency}. It is clear that train $t_2$ may not start its activities before $t_1$, even if $t_2$ has a higher priority.
        
        To model this, let us state $TSB \subset TS$ as the set of all first track segments, $N_{tsb}$ as the number of trains beginning their itinerary on track segment $tsb$, and $AB^{tsb}_i$ as the sequence of first activities of trains $t_i$ beginning on $tsb$ with $i \in [1, N_{tsb}]$, ordered by estimated arrival time of trains $t_i$. Then, the train ordering consistency constraint can be modelled as shown in Equation~\ref{eq:ordering_consistency}.
        
        \begin{align}
            \label{eq:ordering_consistency}
            s(AB^{tsb}_{i-1}) < s(AB^{tsb}_{i}) \hspace{5mm} \forall tsb \in TSB, \forall i \in ]1, N_{tsb}]
        \end{align}
        
    \section{Objective functions}
    This section presents the objective functions used on homogeneous (when there is only one category of trains) and heterogeneous (when we have multiple categories of trains) models.
        \subsection{Homogeneous model}
        The most frequent criterion used as objective function in this case is the sum of train delays. To express this, let us first define $jct_t$ as the \textit{journey completion time of train t}. It is equal to the end date of the last element activity of train \textit{t}. The delay $d_t$ of train $t$ can thus be expressed as: 
        
        \begin{align}
            \label{eq:min_delay_homo}
            d_t = max(0, jct_t - pct_t)
        \end{align}
        
        The \textit{max} function is used to nullify the cases where trains reach their destinations in advance. From Equation~\ref{eq:min_delay_homo}, we can state the objective function of the homogeneous model as:
        
        \begin{align}
            \label{eq:obj_homo}
            min\Big(\sum_{t \in T} d_t\Big)
        \end{align}
        
        
        \subsection{Heterogeneous model}
        \label{subsection:init_model_hetero}
        However, in reality, the operators must consider different categories of trains introduced in Table~\ref{tab:params}  having different priorities. The four considered categories of trains are listed below in decreasing order of priority:
        
        \begin{enumerate}
            \item Maintenance trains (\textit{C1}).
            \item Passenger trains with correspondence (\textit{C2}).
            \item Passenger trains without correspondence (\textit{C3}).
            \item Freight trains (\textit{C4}).
        \end{enumerate}
        
        Moreover, there is now the possibility to take the number of passengers inside the trains into consideration. The objective function becomes then threefold:
        
        \begin{itemize}
            \item Schedule trains according to their priority. 
            \item Minimise the overall delay.
            \item Maximise the overall passenger satisfaction which decreases if their train is delayed.
        \end{itemize}
        
        We end up with the following objective function: 
        
        \begin{align}
            \label{eq:obj_hetero}
            \texttt{lex\_min}\Big(\sum_{t \in C1} d_t, \sum_{t \in C2} p_t\times d_t, \sum_{t \in C3} p_t\times d_t, \sum_{t \in C4} d_t \Big)
        \end{align}
        
         The Equation~\ref{eq:obj_hetero} gives a lexicographical ordering of trains according to their categories and priorities. Remember that $p_t$ stands for the number of passengers inside train $t$. Thus, we are taking into account the number of passengers in the minimisation of the delay for categories \textit{C2} and \textit{C3}. Finally, the objective is to minimise the expression while respecting the lexicographical ordering.
         
         \section{Experiments and results}
         To sum up this chapter, the model has been experimented in~\cite{cappart_rescheduling_2017} on a subset of a Belgian train station (Courtrai). The main results are as follows:
         \begin{itemize}
             \item The CP model was compared against classical scheduling approaches, namely first come first served (FCFS), highest delay first served (HDFS) and highest priority first served (HPFS). 
             
             In the end, the homogeneous model gave an average improvement ratio of 20\% in terms of objective value compared to HDFS and FCFS with a decision time of 3 minutes for instances between 5 and 30 trains. It gave better or equal results than the classical approaches in more than 99\% of the cases. The optimum was reached more than 75\% of the time when considering instances of 10 trains or less.
             
             The heterogeneous model also gave better results in almost all the cases (99\% of the instances) when compared to HPFS with a decision time of 3 minutes and instances between 5 and 30 trains. 
             
             \item The solution is scalable - it allows to obtain competitive results in less than three minutes for instances up to thirty trains.
             
             \item The model is also nice in critical situations. It was tested with a decision time of 10 seconds against HDFS and FCFS and still gave better or equal results in 99\% of the cases.
         \end{itemize}