\label{chapter:CTI}
This chapter presents a type of variables for constraint programming that were first introduced by Laborie and Rogerie~\cite{laborie_reasoning_2008}, conditional time-interval variables. These variables facilitate the modeling of a range of scheduling problems. After an overview of the general principle, a selection of possible constraints is presented, followed by an introduction to the propagation on these constraints.

\section{General principle}
 A conditional time-interval variable represents the time-interval of an activity whose execution status can take two values : 
 \begin{enumerate}
     \item Executed: the activity represented by the variable is executed~;
     \item Not executed: the activity is not executed and constraints do not consider the variable.
 \end{enumerate}
 
 More formally, a time-interval variable $v$ has a domain $\{[s, e) \mid s, e \in \mathbb{Z}, s \leq e\}$, where $s$ is an integer that stands for the start time of the activity, and $e$ is an integer that stands for its end time. This is in the case of a mandatory activity, \textit{i.e.} an activity that has to be executed. 
 
 A \textit{conditional} time-interval variable $i$ has a domain $\{\bot\} \cup \{[s, e) \mid s, e \in \mathbb{Z}, s \leq e\}$ and takes the value
 \begin{itemize}
     \item $i = \bot$ if the activity is not executed~;
     \item $i = [s, e)$ if the activity is executed, starts at time $s$ and ends at time $e$. 
 \end{itemize}
 
Hereafter, conditional time-interval variables will be referred to as CTI variables. The execution status of a CTI variable $i$ is denoted $x(i)$ and can take value $0$ (not executed) or $1$ (executed). Without other information, $x(i)$ means that the variable is executed, and $\neg x(i)$ means that it is not. Similarly, the start and end of $i$ are denoted $s(i)$ and $e(i)$ and can take any of the values from their initial domain.
 
 
\section{Constraints}

Theses variables are constituted of two dimensions: logical (execution status) and temporal (start and end times). 

Constraints on CTI variables can therefore be classified according to these categories: constraints on the logical dimension, and constraints on the temporal one. Some hybrid constraints also restrict values from both dimensions. Most of these constraints and their semantic were introduced along with the CTI concept by Laborie and Rogerie~\cite{laborie_reasoning_2008}.

\subsection{Logical constraints}

These constraints restrict the execution status of the variables that take part in it.

\subsubsection*{Execution}

This constraint simply states that the activity corresponding to the interval has to be executed.
$$exec(i) \leftrightarrow x(i) = 1$$

\subsubsection*{Implication}

This binary constraint between CTI variables $i$ and $j$ states that, if activity $i$ is executed, activity $j$ has to be executed. 
$$implication(i, j) \leftrightarrow x(i) = 1 \implies x(j) = 1$$

\subsubsection*{Equivalence}

This global constraint states that each CTI variable $\{i_1, ..., i_n\}$ it constrains has to have the same execution status.
$$equivalence(i_1, ..., i_n) \leftrightarrow x(i_1) \equiv ... \equiv x(i_n)$$

\subsection{Temporal constraints}

These constraints restrict the time boundaries of the variables, and take only variables that are executed into account.

\subsubsection*{Start at start}

This constraint between CTI variables $i$ and $j$ states that, if both activities are executed, $i$ has to have the same start time as $j$.
$$startAtStart(i, j) \leftrightarrow x(i) \wedge x(j) \implies s(i) = s(j)$$

It is also possible to specify a gap value $z \in \mathbb{Z}$ such that
$$startAtStart(i, j, z) \leftrightarrow x(i) \wedge x(j) \implies s(i) + z = s(j)$$

The next few constraints follow the same pattern.

\subsubsection*{Start at end}
$$startAtEnd(i, j) \leftrightarrow x(i) \wedge x(j) \implies s(i) = e(j)$$
$$startAtEnd(i, j, z) \leftrightarrow x(i) \wedge x(j) \implies s(i) + z = e(j)$$

\subsubsection*{End at start}
$$endAtStart(i, j) \leftrightarrow x(i) \wedge x(j) \implies e(i) = s(j)$$
$$endAtStart(i, j, z) \leftrightarrow x(i) \wedge x(j) \implies e(i) + z = s(j)$$

\subsubsection*{End at end}
$$endAtEnd(i, j) \leftrightarrow x(i) \wedge x(j) \implies e(i) = e(j)$$
$$endAtEnd(i, j, z) \leftrightarrow x(i) \wedge x(j) \implies e(i) + z = e(j)$$

\noindent These constraints also exist in less restrictive forms, in which the strict equality between time boundaries is replaced by an ordered inequality relation.

\subsubsection*{Start before start}
$$startBeforeStart(i, j) \leftrightarrow x(i) \wedge x(j) \implies s(i) \leq s(j)$$
$$startBeforeStart(i, j, z) \leftrightarrow x(i) \wedge x(j) \implies s(i) + z \leq s(j)$$

\subsubsection*{Start before end}
$$startBeforeEnd(i, j) \leftrightarrow x(i) \wedge x(j) \implies s(i) \leq e(j)$$
$$startAtEnd(i, j, z) \leftrightarrow x(i) \wedge x(j) \implies s(i) + z \leq e(j)$$

\subsubsection*{End before start}
$$endBeforeStart(i, j) \leftrightarrow x(i) \wedge x(j) \implies e(i) \leq s(j)$$
$$endBeforeStart(i, j, z) \leftrightarrow x(i) \wedge x(j) \implies e(i) + z \leq s(j)$$

\subsubsection*{End before end}
$$endBeforeEnd(i, j) \leftrightarrow x(i) \wedge x(j) \implies e(i) \leq e(j)$$
$$endBeforeEnd(i, j, z) \leftrightarrow x(i) \wedge x(j) \implies e(i) + z \leq e(j)$$

\subsubsection*{No overlap}
\label{subsub:nooverlap}
This global constraint on $\{i_1, ..., i_n\}$ is useful for unary resources that can handle at most one activity at a given time and is introduced by Laborie et al.~\cite{laborie_reasoning_2009}. It enforces the fact that there cannot be any overlap between executed intervals in $\{i_1, ..., i_n\}$. 
$$noOverlap(i_1, ..., i_n) \leftrightarrow \forall (j, k)_{\;j, k \in [1,n] \; \land \; j \neq k \; \land \; x(j) \; \land \; x(k)}\quad e(i_j) \leq s(i_k) \; \lor \; e(i_k) \leq s(i_j)$$

Specifying a transition time between activities is also possible via a transition time matrix $M$. The constraint is then
$$noOverlap(i_1, ..., i_n, M) \leftrightarrow$$
$$\forall (j, k)_{\;j, k \in [1,n] \; \land \; j \neq k \; \land \; x(j) \; \land \; x(k)}\quad e(i_j) + M(j, k) \leq s(i_k) \; \lor \; e(i_k) + M(k, j)\leq s(i_j)$$

\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{images/overlap.png}
    \caption{Overlap between activities a and b}
    \label{fig:overlap}
\end{figure}



\subsection{Hybrid constraints}

\subsubsection*{Span}
\label{subsub:span}
This global constraint on $i$ and $\{j_1, ..., j_n\}$ enforces restrictions on logical and temporal values of the variables that are involved in it. It states that, if $i$ is executed, then $i$ starts with the earliest starting executed variable in $\{j_1, ..., j_n\}$ and ends with the latest ending executed variable in $\{j_1, ..., j_n\}$. $i$ is not executed if and only if no variable in $\{j_1, ..., j_n\}$ is executed.


$$span(i, \{j_1, ..., j_n\}) \leftrightarrow \left\{
    \begin{array}{l}
        \neg x(i) \ \Leftrightarrow \ \forall k \in [1,n] \; \neg x(j_k) \\
        x(i) \Leftrightarrow \left\{
        \begin{array}{l}
            \exists k \in [1,n] \quad x(j_k)\\
            s(i) \ = \ min_{k \in [1,n]} \ s(j_k)\\
            e(i) \ = \ max_{k \in [1,n]} \ e(j_k)
        \end{array}
        \right.
    \end{array}
    \right.$$
    
\subsubsection*{Alternative}
\label{subsub:alternative}
This constraint on $i$ and $\{j_1, ..., j_n\}$ is similar to the span constraint apart from the fact that $i$ is executed if and only if exactly one variable in $\{j_1, ..., j_n\}$ is executed. The time boundaries of $i$ and that executed variable are then equal.


$$alternative(i, \{j_1, ..., j_n\}) \leftrightarrow \left\{
    \begin{array}{l}
        \neg x(i) \ \Leftrightarrow \ \forall k \in [1,n] \quad \neg x(j_k) \\
        x(i) \ \Leftrightarrow \ \exists k \in [1,n] \; \left\{
        \begin{array}{l}
            x(j_k)\\
            s(i) \ = \ s(j_k)\\
            e(i) \ = \ e(j_k)\\
            \forall l \neq k \; \neg x(j_l)
        \end{array}
        \right.
    \end{array}
    \right.$$
    

\section{Propagation}
\label{section:cti_propagation}

The optional nature of these activities implies that the propagation procedures of each of the above constraints have to be adapted. The naive solution is to only propagate domain reductions or value binding of temporal boundaries of a variable if it is assured to be executed ($x(i) = 1$). However, this means that the execution status of a variable has to be bound before any domain reduction on its temporal dimension can happen. 

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{images/legend_activity.png}
    \caption{Representation of type of activities}
    \label{fig:legend_activity}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{images/legend_links.png}
    \caption{Representation of links between activities}
    \label{fig:legend_link}
\end{figure}


An example of a case is represented in Figure~\ref{fig:before_propagation}. The activities and their links are graphically represented according to the convention depicted in Figures~\ref{fig:legend_activity} and \ref{fig:legend_link}.

It represents a case where the main activity is to wash clothes. This activity is mandatory and is therefore pictured as a box with a solid border. Possible values for start and end boundaries are written at each extremity of the box.

There are two possibilities for this activity: hand wash the clothes or put them in the washing machine. This is represented by the XOR link between them and the main activity. As these alternatives are not mandatory, they are depicted as boxes with dotted borders. Furthermore, the hand wash option is composed of several sub-activities (span constraint), where every sub-activity has to be executed if the spanned activity is executed (represented by the logical links between the spanned activity and its components). These sub-activities have a fixed duration and are linked with temporal restrictions (end before start constraints), as they have to be executed in that particular order.


\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{images/before_propagation.png}
    \caption{Initial state of the activities}
    \label{fig:before_propagation}
\end{figure}

The naive propagator that only takes executed activities into account cannot reduce a single domain in this situation, as only one activity is assured to be executed. However, a lot of information can be exploited in order to propagate and get the result shown in Figure~\ref{fig:after_propagation}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{images/after_propagation.png}
    \caption{State of the activities after propagation}
    \label{fig:after_propagation}
\end{figure}

This result can be achieved by the addition of two network-like structures that help the propagation, a logical and a temporal network, as described by Laborie and Rogerie\cite{laborie_reasoning_2008}.

\subsubsection*{Logical network}

This structure is a graph that contains information on links between execution status of CTI variables. For variable $i$, two nodes are possible: $x(i)$ and $\neg x(i)$. An edge between nodes $n$ and $m$ states that there exists an implication $n \implies m$. For the propagation to be possible, the network needs to have these properties:
\begin{itemize}
    \item Detection of inconsistencies in the network (when the network infers $n \implies \neg n$ and $\neg n \implies n$)~;
    \item Access to the logical relation between any pair of activities~;
    \item Access to the activities implied by an activity, and to the activities that imply an activity~;
    \item Detection of new logical relations and the activation of events accordingly.
\end{itemize}

\subsubsection*{Temporal network}

As its logical counterpart, this is also a graph, which contains information on temporal links between activities. The nodes of this graph are temporal boundaries of activities, $i.e.$ for an activity $i$, two nodes will be present in the graph: $s(i)$ and $e(i)$. This graph is weighted, and an edge of weight $w$ between two nodes $n$ and $m$ corresponds to the relation $n + w \leq m$.

To propagate efficiently, the temporal network takes advantage of the logical relations inferred by the logical network.